<?php

namespace App\Repository;

use App\Entity\Tasks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Tasks>
 *
 * @method Tasks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tasks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tasks[]    findAll()
 * @method Tasks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TasksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tasks::class);
    }

    public function add(Tasks $entity, bool $flush = false): void
    {
        if (!$entity->getCreateDate()){
           $entity->setCreateDate(new \DateTime());
        }
        $entity->setUpdateDate(new \DateTime());

        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Tasks $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->createQueryBuilder('t')->update()
                ->set('t.parentTaskId',':parent_task_id')
                ->setParameter('parent_task_id',null)
                ->where('t.parentTaskId = :id')
                ->setParameter('id',$entity->getId())
                ->getQuery()
                ->execute();
            $this->getEntityManager()->flush();
        }
    }
}
