<?php

namespace App\Entity\Tasks;

use App\Entity\Tasks;

class Factory
{
    public function create($data) :Tasks
    {
        return (new Tasks())
            ->setTitle($data['title'])
            ->setDescription($data['description'])
            ->setPriority($data['priority'])
            ->setStatus($data['status'])
            ->setParentTaskId($data['parent_task_id']);
    }
}