<?php

namespace App\Controller;

use App\Services\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/tasks')]
class TasksController extends AbstractController
{


    public function __construct(
        private readonly Task $task
    )
    {
    }

    #[Route('/', name: 'app_tasks_index', methods: ['POST'])]
    public function index(Request $request): JsonResponse
    {
        $payload = json_decode($request->getContent(), true);
        $result=[];
        foreach ($this->task->showList($payload) as $entity){
            $result[]=$entity->toArray();
        }
        return new JsonResponse($result);
    }

    #[Route('/add', name: 'app_tasks_add', methods: ['POST'])]
    public function add(Request $request): Response
    {
        $payload = json_decode($request->getContent(), true);

        return new JsonResponse($this->task->add($payload));

    }

    #[Route('/{id}/edit', name: 'app_tasks_edit', methods: ['POST'])]
    public function edit(Request $request, int $id): Response
    {
        $payload = json_decode($request->getContent(), true);
        return new JsonResponse($this->task->edit($payload, $id));
    }

    #[Route('/{id}/changeStatus', name: 'app_tasks_changeStatus', methods: ['POST'])]
    public function changeStatus(Request $request, int $id): Response
    {
        $payload = json_decode($request->getContent(), true);
        return new JsonResponse($this->task->changeStatus($payload, $id));
    }

    #[Route('/{id}/delete', name: 'app_tasks_delete', methods: ['POST'])]
    public function delete(Request $request, int $id): Response
    {
        $payload = json_decode($request->getContent(), true);
        return new JsonResponse($this->task->delete($id));
    }
}
