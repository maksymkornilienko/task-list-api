<?php

namespace App\Services;

use App\Entity\Tasks;
use App\Entity\Tasks\Factory;
use App\Repository\TasksRepository;
use App\Services\Task\Filters;
use App\Services\Task\Sorts;
use Doctrine\Persistence\ManagerRegistry;

class Task
{
    public function __construct(
        private readonly TasksRepository $repository,
        private readonly Factory         $tasksFactory,
        private readonly Filters         $filters,
        private readonly Sorts           $sorts
        )
    {
    }

    /**
     * @param array $payload
     * @return Tasks[]
     */
    public function showList(array $payload):array
    {
        $queryBuilder = $this->repository->createQueryBuilder('t');

        $filters=array_key_exists('filters',$payload)?$payload['filters']:[];
        foreach ($filters as $name => $value){
            $queryBuilder = $this->filters->process($queryBuilder, $name, $value);
        }

        $sorts=array_key_exists('sorts',$payload)?$payload['sorts']:[];
        foreach ($sorts as $name => $value){
            $queryBuilder = $this->sorts->process($queryBuilder, $name, $value);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function add(array $payload)
    {
        $entity=$this->tasksFactory->create($payload);

        return $this->save($entity);
    }

    public function edit(array $payload,int $id)
    {
        $entity=$this->repository->find($id);
        if (!$entity){
            return json_encode(sprintf('Does not exist property'));
        }

        $entity->setParentTaskId($payload['parent_task_id'])
            ->setPriority($payload['priority'])
            ->setDescription($payload['description'])
            ->setTitle($payload['title']);

        return $this->save($entity);
    }

    public function changeStatus(array $payload,int $id)
    {
        $entity=$this->repository->find($id);
        if (!$entity){
            return json_encode(sprintf('Does not exist property'));
        }

        if ($payload['status'] === $entity->getStatus()){
            return '';
        }

        $data = $this->repository->createQueryBuilder('t')->select('t.status as status')->where('t.')->groupBy('t.status');
        foreach ($data as $item){
            if ($item==='todo'){
                return json_encode(sprintf('You have todo statuses in subtasks'));
            }
        }

        $entity->setStatus($payload['status']);

        return $this->save($entity);
    }
    public function delete(int $id)
    {
        $entity=$this->repository->find($id);
        if ($entity->isDone()){
            return json_encode(sprintf('error "%s": ','You cannot delete tasks with done status'));
        }
        try {
            $this->repository->remove($entity,true);
            return json_encode('success');
        }catch (\Exception $e){
            return json_encode(sprintf('error "%s": ',$e->getMessage()));
        }
    }
    private function save($entity):string
    {
        try {
            $this->repository->add($entity,true);
            return json_encode('success');
        }catch (\Exception $e){
            return json_encode(sprintf('error "%s": ',$e->getMessage()));
        }
    }
}