<?php

namespace App\Services\Task;

use App\Services\Task\Sorts\SortInterface;
use Doctrine\ORM\QueryBuilder;

class Sorts
{
    private const SORT_LIST=[
        'createDate'=>\App\Services\Task\Sorts\CreateDate::class,
        'updateDate'=>\App\Services\Task\Sorts\UpdateDate::class,
        'priority'=>\App\Services\Task\Sorts\Priority::class,
    ];
    public function process(QueryBuilder $queryBuilder, string $name, $value){
        $class=self::SORT_LIST[$name];
        /**
         * @var SortInterface $class
         */
        $class=new $class();

        return $class->apply($queryBuilder, $value);
    }
}