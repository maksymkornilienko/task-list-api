<?php

namespace App\Services\Task;

use App\Services\Task\Filters\FilterInterface;
use Doctrine\ORM\QueryBuilder;

class Filters
{
    private const FILTER_LIST=[
        'title'=>\App\Services\Task\Filters\Title::class,
        'status'=>\App\Services\Task\Filters\Status::class,
        'priority'=>\App\Services\Task\Filters\Priority::class,
    ];
    public function process(QueryBuilder $queryBuilder, string $name, $value){

        $class=self::FILTER_LIST[$name];
        /**
         * @var FilterInterface $class
         */
        $class=new $class();
        return $class->apply($queryBuilder, $value);
    }
}