<?php

namespace App\Services\Task\Filters;

use Doctrine\ORM\QueryBuilder;

class Priority implements FilterInterface
{

    public function apply(QueryBuilder $queryBuilder, $value)
    {
        $queryBuilder->andWhere('t.priority >= :from')
                     ->andWhere('t.priority <= :to')
                     ->setParameter('from', $value['from'])
                     ->setParameter('to', $value['to']);

        return $queryBuilder;
    }
}