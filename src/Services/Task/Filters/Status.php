<?php

namespace App\Services\Task\Filters;

use Doctrine\ORM\QueryBuilder;

class Status implements FilterInterface
{
    public function apply(QueryBuilder $queryBuilder, $value)
    {
        $queryBuilder->andWhere('t.status = :status')
            ->setParameter('status', $value);

        return $queryBuilder;
    }
}