<?php

namespace App\Services\Task\Filters;

use Doctrine\ORM\QueryBuilder;

interface FilterInterface
{
    public function apply(QueryBuilder $queryBuilder, $value);
}