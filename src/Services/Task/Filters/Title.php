<?php

namespace App\Services\Task\Filters;

use Doctrine\ORM\QueryBuilder;

class Title implements FilterInterface
{

    public function apply(QueryBuilder $queryBuilder, $value)
    {
        $queryBuilder->andWhere("t.title LIKE :title")
            ->setParameter('title', '%' . $value . '%');
        return $queryBuilder;
    }
}