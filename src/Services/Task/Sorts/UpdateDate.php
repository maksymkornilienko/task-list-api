<?php

namespace App\Services\Task\Sorts;

use Doctrine\ORM\QueryBuilder;

class UpdateDate implements SortInterface
{

    public function apply(QueryBuilder $queryBuilder, $value)
    {
        $queryBuilder->addOrderBy('t.updateDate', $value);

        return $queryBuilder;
    }
}