<?php

namespace App\Services\Task\Sorts;

use Doctrine\ORM\QueryBuilder;

interface SortInterface
{
    public function apply(QueryBuilder $queryBuilder, $value);
}