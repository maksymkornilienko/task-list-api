<?php

namespace App\Services\Task\Sorts;

use Doctrine\ORM\QueryBuilder;

class Priority implements SortInterface
{

    public function apply(QueryBuilder $queryBuilder, $value)
    {
        $queryBuilder->addOrderBy('t.priority', $value);

        return $queryBuilder;
    }
}