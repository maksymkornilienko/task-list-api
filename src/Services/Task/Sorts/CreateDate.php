<?php

namespace App\Services\Task\Sorts;

use Doctrine\ORM\QueryBuilder;

class CreateDate implements SortInterface
{

    public function apply(QueryBuilder $queryBuilder, $value)
    {
        $queryBuilder->addOrderBy('t.createDate', $value);

        return $queryBuilder;
    }
}